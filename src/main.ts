import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router/index";
import { BreadcrumbPlugin } from "@hcd-bsas/vue3-components";

import "./assets/main.scss";
import "./assets/tooltip.scss"; // Esto es necesario dado que estando en intranet no podemos importar la libreria de bootstrap no modificado
import "@hcd-bsas/vue3-components/dist/style.css";

const pinia = createPinia();

const app = createApp(App);

app.use(pinia);
app.use(router);
app.use(BreadcrumbPlugin, { router: router });
app.mount("#hcd_becasnuevo");
