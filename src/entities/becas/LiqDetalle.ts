export default class LiqDetalle {
	id_liq_deta!: number;
	id_liq_cab!: number;
	id_liq_resu!: number;
	id_beca!: number;
	f_periodo!: string;
	mes_pag?: number;
	importe!: number;
	importe_total?: number;
}