import { AutocompleteElement } from "@hcd-bsas/vue3-components";
export default class Becario extends AutocompleteElement {

	id_becario!: number;
	tipo_doc!: string;
	doc?: string;
	apellido!: string;
	nombres!: string;
	sexo!: string;
	f_nacimiento!: string;
	calle_dom!: string;
	nro_dom!: string;
	piso_dom!: string;
	depto_dom!: string;
	id_localidad!: number;
	localidad!: string;
	id_partido!: number;
	partido!: string;
	id_seccion?: number;
	seccion?: string;
	cod_postal?: string;
	cuil!: string;
	baprosuc!: number;
	cbu_completo?: string;
	estado!: string;
	id_baproestado!: number;
	id_beca?: number;
	f_visado_beca_min!: string;
	msj_validacion?: string;

	get key(): string | number {
		return this.id_becario;
	}
	get value(): string {
		return this.apellido + ' ' + this.nombres;
	}

}