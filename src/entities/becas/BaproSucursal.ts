import { AutocompleteElement } from "@hcd-bsas/vue3-components";
export default class BaproSucursal extends AutocompleteElement {

	c_bapro!: string;
	d_bapro!: string;
	direccion!: string;
	codigo_pos!: string;
	localidad!: string;
	partido!: string;
	ciudad!: string;
	pais!: string;
	prefijo!: string;
	te_gcia_1!: string;
	contad_1!: string;
	fax_gcia!: string;
	fax_contad!: string;
	linea_dire!: string;
	dir_andreani!: string;
	loc_andreani!: string;
	cp_andreani!: string;

	get key(): string | number {
		return this.c_bapro;
	}
	get value(): string {
		return this.c_bapro + ' - ' + this.d_bapro;
	}

}