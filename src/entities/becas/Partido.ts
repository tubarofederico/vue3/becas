import { AutocompleteElement } from "@hcd-bsas/vue3-components";

export default class Partido extends AutocompleteElement {

	id_partido!: number;
	partido!: string;
	id_provincia!: number;
	id_seccion!: number;

	get key(): string | number {
		return this.id_partido;
	}
	get value(): string {
		return this.partido;
	}

}