export default class VisadoHist {
	id_beca_historico!: number;
	id_beca!: number;
	visado?: string;
	f_visado?: string;
	visador?: string;
	f_vencim_v?: string;
	f_segu?: string;
	clave?: string;
}