import { AutocompleteElement } from "@hcd-bsas/vue3-components";

export default class Localidad extends AutocompleteElement {

	id_localidad!: number;
	localidad!: string;
	id_partido!: number;
	cod_postal!: string;

	get key(): string | number {
		return this.id_localidad;
	}
	get value(): string {
		return this.localidad;
	}

}