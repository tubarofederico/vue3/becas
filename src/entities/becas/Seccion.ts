import { AutocompleteElement } from "@hcd-bsas/vue3-components";

export default class Seccion extends AutocompleteElement {

	id_seccion!: number;
	seccion!: string;

	get key(): string | number {
		return this.id_seccion;
	}
	get value(): string {
		return this.seccion;
	}

}

/*
export default class Seccion {
	id_seccion!: number;
	seccion!: string;
}
*/