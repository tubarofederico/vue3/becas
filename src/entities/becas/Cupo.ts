export default class Cupo {
	id_resumen_solicitante!: number;
	fecha_cupo!: string;
	id_tabla_sol!: number;
	apellido!: string;
	nombres!: string;
	estado!: string;
	monto_otorgado!: number;
	monto_gastado!: number;
	monto_liquidado!: number;
	monto_disponible!: number;
	transfiere: number = 0;
	valido: boolean = true;
}