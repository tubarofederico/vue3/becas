export default class Cupo {
	id_becariocupo!:number;
	descripcion!:string;
	monto_min!:number | null;
	monto_max!:number | null;
	vigencia_desde!:string;
	vigencia_hasta!:string;
}