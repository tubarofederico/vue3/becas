import { AutocompleteElement } from "@hcd-bsas/vue3-components";
export default class Bloque extends AutocompleteElement {

	d_dependen!: string;
	id_dependen!: number;

	get key(): number {
		return this.id_dependen;
	}
	get value(): string {
		return this.d_dependen;
	}


}