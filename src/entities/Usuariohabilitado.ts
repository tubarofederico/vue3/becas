export class Usuariohabilitado {
	id_usuario_perfil!:number;
	id_usuario!:string;
	tipo_doc!:string;
	nro_doc!:number;
	ayn_usuario!:string;
	d_dependen_n!:string;
	id_perfil!:number;
	id_perfil_consulta!:number;
	perfil!:string;
	id_solicitante!:number;
	id_tabla_sol!:number;
	d_solicitante!:string;
}