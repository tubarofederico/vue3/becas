export default class Safin {
	archivo!: string;
	cantidad!: number | null;
	clave!: string;
	f_segu!: string;
	id_liq_resu!: number;
	d_tipo_proceso!: string;
	tipo_proceso!: string;
	id_safincab!: number;
	d_liq!: string;
}