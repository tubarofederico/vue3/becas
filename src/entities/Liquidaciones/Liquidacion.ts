import { AutocompleteElement } from "@hcd-bsas/vue3-components";
export default class Liquidacion extends AutocompleteElement {
	id_liq_resu!: number | null;
	f_periodo!: string;
	tipo_liq!: string;
	d_tipo_liq!: string;
	nro_liq!: number | null;
	f_cierre!: string;
	clave_cierre!: string;
	nro_opago!: number | null;
	estado!: string;
	d_estado!: string;
	f_segu!: string;
	clave!: string;
	perimmaa!: string;

	get key(): string | number {
		return this.id_liq_resu!;
	}
	get value(): string {
		return 'Periodo: ' + this.f_periodo + ' ' + this.d_tipo_liq + ' Nro: ' + this.nro_liq;
	}

}