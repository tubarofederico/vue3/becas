export default class CabeceraBecas {
	id_liq_cab!: number | null;
	expediente!: string;
	apyn!: string;
	doc!: string;
	apyn_apoderado!: string;
	doc_apoderado!: string;
	Importe_total!: number;
	apyn_sol!: string;
	nro_liq!: number;
	f_periodo!: string;
	nro_opago!: number;
	f_segu!: string;
	d_dependen!: string;
	apyn_sol_resp!: string;
	baprosuc!: number;
	f_anulada!: string;
}




