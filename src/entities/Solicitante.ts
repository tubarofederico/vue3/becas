import { AutocompleteElement } from "@hcd-bsas/vue3-components";
export default class Solicitante extends AutocompleteElement {

	nro_doc!: number;
	tipo_doc!: number;
	ayn!: string;
	periodo!: string;
	d_dependen!: string;
	sexo!: string;
	cod_safin!: string;
	id_seccion!: number;
	sec_electoral!: string;
	id_cargo!: number;
	id_dependencia!: number;
	partidas!: number;
	id_solicitante!: number;
	proyecto!: number;
	d_cargo!: string;
	estado!: string;
	id_tabla_sol!: number;
	id_tabla_sol_reemplaza!: number;
	ayn_reemplaza!: string;
	legajo!: number;
	id_tabla_sol_reemp!: number;
	tot_cupoytransf!: number;
	tot_gastado!: number;
	tot_liquidado!: number;
	tot_anulado!: number;
	tot_disponible!: number;

	get key(): string | number {
		return this.id_tabla_sol;
	}
	get value(): string {
		return this.ayn;
	}

}