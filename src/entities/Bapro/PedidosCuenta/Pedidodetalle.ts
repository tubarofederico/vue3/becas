export default class Pedidodetalle {
	id_baprocab!:number;
	archivo!:string;
	cantidad!:number | null;
	f_genera!:string;
	f_segu!:string;
	clave!:string;
	id_baprodeta!:number;
	id_becario!:number;
	tipo_doc!:string;
	doc!:string;
	apellido!:string;
	nombres!:string;
	sexo!:string;
	f_nacimiento!:string;
	calle_dom!:string;
	nro_dom!:string;
	piso_dom!:string;
	depto_dom!:string;
	id_localidad!:number;
	id_partido!:number;
	cod_postal!:number;
	cuil!:number;
	baprosuc!:string;
	cbu!:number;
	cbu_bloq1!:number;
	cbu_bloq2!:number;
	esado!:string;
	id_baproestado!:number;
	apyn!:string;
	d_baprosuc!:string;
	sucursal!:string;

}



                
    