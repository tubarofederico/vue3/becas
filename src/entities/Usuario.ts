import { AutocompleteElement } from "@hcd-bsas/vue3-components";
export default class Usuario extends AutocompleteElement {

	id_usuario!:string;
	tipo_doc!:string;
	nro_doc!:number | null;
	ayn_usuario!:string;
	d_dependen_N!:string;

	get key(): string {
		return this.id_usuario;
		}

		get value(): string {
			return this.id_usuario + "("+ this.ayn_usuario.trim() +")";
		}	

}