import { AutocompleteElement } from "@hcd-bsas/vue3-components";
export default class Tipotransf extends AutocompleteElement {

	tipo_transf!:string;
	d_tipo_transf!:string
	clave!:string;
	f_segu!:string;

	get key(): string {
		return this.tipo_transf;
	}
	get value(): string {
		return this.d_tipo_transf;
	}


}