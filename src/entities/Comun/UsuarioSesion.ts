export default class UsuarioSesion {
	id_usuario!: string | null;
	doc!: string | null; // nueve caracteres en total: uno para el tipo de documento y ocho para el número
	grupo_usu!: string | null;
	id_dependen!: number | null;
}