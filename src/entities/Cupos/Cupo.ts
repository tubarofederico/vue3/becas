export default class Cupo {
	id_cupo_movimiento_cabecera!: number;
	id_tabla_sol!: number | null;
	id_cargo!: number;
	f_periodo!: string;
	tipo_transf!: string;
	d_tipo_transf!: string;
	f_transf!: string;
	importe!: number;
	id_tabla_sol_reemp!: number | null;
	id_transferencia!: number | null;
	observ!: string;
	d_cargo!: string;
	f_desde!: string;
	f_hasta!: string;
	fecha_cupo!: string;
	importe_mes!: number;
	tot_bec_subs!: number;
	tot_asig_subs!: number;
	tot_subs_bec!: number;
	tot_asig_bec!: number;
	tot_ajuste!: number;
	tot_otras!: number;
	apyn_sol!: string;
	ayn_solicitante!:string;
}