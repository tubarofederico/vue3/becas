import { AutocompleteElement } from "@hcd-bsas/vue3-components";
export default class Cargo extends AutocompleteElement {

	id_cargo!:string;
	id_tabla_sol!:string | null;
	d_cargo!:string;
	id!:number;

/* 	get key(): number {
		//return this.id_cargo;
		return this.id;
		} */


	get key(): string {
		return this.id_cargo + this.id_tabla_sol;
		}	

		get value(): string {
			return this.d_cargo;
		}	

}