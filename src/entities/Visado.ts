export default class Visado {
	id_visado!: number | null | string;
	c_visado!: string;
	d_visado!: string;
}