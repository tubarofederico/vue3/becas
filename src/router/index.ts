import {
  createRouter,
  createWebHashHistory,
  type RouteRecordRaw,
} from "vue-router";

// VISTAS USUARIOS
import Inicio from "@/views/Inicio.vue";
import InicioUsuarios from "@/views/usuarios/InicioUsuarios.vue";
import EdicionUsuarios from "@/views/usuarios/EdicionUsuarios.vue";

// VISTAS CUPOS
import InicioCuposPorCargo from "@/views/cupos/InicioCuposPorCargo.vue";
import EdicionCupos from "@/views/cupos/EdicionCupos.vue";
import InicioTopesMensuales from "@/views/cupos/InicioTopesMensuales.vue";
import EdicionTopes from "@/views/cupos/EdicionTopes.vue";
import InicioAjustePorBajaSolicitante from "@/views/cupos/InicioAjustePorBajaSolicitante.vue";
import EdicionTransferenciasAjustePorBajaSolicitante from "@/views/cupos/EdicionTransferencias.vue";

// VISTAS TABLA LIQUIDACIONES
import HabilitarCerrar from "@/views/liquidaciones/HabilitarCerrar.vue";
import InicioProceso from "@/views/liquidaciones/InicioProceso.vue";

//VISTAS BAPRO
import inicioPedidosCuenta from "@/views/bapro/pedidocuentas/InicioPedidosCuenta.vue";
import inicioPedidosArchivo from "@/views/bapro/pedidocuentas/InicioPedidosArchivo.vue";
import inicioLecturaCuentas from "@/views/bapro/lecturacuentas/inicioLecturaCuentas.vue";
import InicioConsultaCuentas from "@/views/bapro/consultacuentas/InicioConsultaCuentas.vue";

// VISTAS BECAS (ACTUALIZACIÓN Y VISADO)
import BecasActualizacion from "@/views/becas/actualizacion/Inicio.vue";
import BecasActualizacionAbm from "@/views/becas/actualizacion/abm/Abm.vue";

// VISTAS SOLICITANTES (CONSULTA)
import ConsSolicitantes from "@/views/solicitantes/consulta/Inicio.vue";

// VISTAS TRANSFERENCIAS
import inicioTransferencias from "@/views/transferencias/InicioTransferencias.vue";
import EdicionTransferencias from "@/views/transferencias/EdicionTransferencias.vue";


// VISTAS CONSULTA LIQUIDACION
import ConsultaLiquidacion from "@/views/liquidaciones/ConsultaLiquidacion.vue";

// VISTAS ANULAR LIQUIDACION
import anularLiquidacion from "@/views/liquidaciones/anularLiquidacion.vue";

// VISTAS INHABILITAR BECAS
import inhabilitarBecas from "@/views/becas/inhabilitarBecas/inhabilitarBecas.vue";

//PERIODO INICIALIZAR 
import Periodo from "@/views/habilitarPeriodo/Periodo.vue";

//VISTAS INFORME BECAS
import inicioInformeBecas from "@/views/becas/informeBecas/inicioInformeBecas.vue";
import inicioInformeBecasDetalle from "@/views/becas/informeBecas/inicioInformeBecasDetalle.vue";

//VISTAS GENERAR ARCHIVO DEPOSITO
import inicioGenerarArchivoDeposito from "@/views/liquidaciones/generarArchivoDeposito/inicioGenerarArchivoDeposito.vue";

// VISTAS INFORME LIQUIDACION
import inicioInformeLiquidacion from "@/views/liquidaciones/informeLiquidacion/InicioInformeLiquidacion.vue";

// VISTAS BAJADA FORMULARIOS
import BajadaFormularios from "@/views/bajadaFormularios/BajadaFormularios.vue";

// VISTAS INFORME TESORERIA
import InformeTesoreria from "@/views/liquidaciones/informeTesoreria.vue";

//VISTAS GENERAR ARCHIVO SAFIN
import inicioArchivoSafin from "@/views/liquidaciones/archivoSafin/InicioArchivoSafin.vue";
import InicioGenerarArchivoSafin from "@/views/liquidaciones/archivoSafin/InicioGenerarArchivoSafin.vue";
import inicioLecturaSafin from "@/views/liquidaciones/archivoSafin/inicioLecturaSafin.vue";


// RUTAS
const routes: Readonly<RouteRecordRaw[]> = [
  {
    path: "/",
    name: "inicio",
    component: Inicio,
    meta: { name: "Inicio Becas Nuevo" }
  },

  // USUARIOS
  {
    path: "/usuarios",
    name: "inicioUsuarios",
    component: InicioUsuarios,
    meta: { name: "Usuarios" },
  },
  {
    path: "/usuarios/editar/:id(\\d+)?",
    name: "editar-usuarios",
    component: EdicionUsuarios,
    meta: { name: "Actualizacion" },
  },

  // RUTAS CUPO
  {
    path: "/cuposporcargo",
    name: "inicioCuposPorCargo",
    component: InicioCuposPorCargo,
    meta: { name: "Cupos Por Cargo" },
  },
  {
    path: "/cupos/inicioTopesMensuales",
    name: "inicioTopesMensuales",
    component: InicioTopesMensuales,
    meta: { name: "Topes Mensuales" },
  },
  {
    path: "/cupos/porcargo/editar/:id(\\d+)?",
    name: "editar-cupos",
    component: EdicionCupos,
    meta: { name: "Actualizacion" },
  },
  {
    path: "/cupos/inicioTopesMensuales/editar/:id(\\d+)?",
    name: "editar-topes",
    component: EdicionTopes,
    meta: { name: "Actualizacion" },
  },
  {
    path: "/cupos/ajuste-baja-solicitante/:id_tabla_sol(\\d+)?",
    name: "ajusteBajaSolicitante",
    component: InicioAjustePorBajaSolicitante,
    meta: { name: "Ajuste por Baja Diputado" },
  },
  {
    path: "/cupos/ajuste-baja-solicitante/editar/:periodo(\\d+)?/:id_tabla_sol(\\d+)?",
    name: "editarTransfBajaSolicitante",
    component: EdicionTransferenciasAjustePorBajaSolicitante,
    meta: { name: "Realizar el Ajuste" },
  },

  //LIQUIDACION
  {
    path: "/liquidacion/proceso",
    name: "inicioProceso",
    component: InicioProceso,
    meta: { name: "Proceso de Liquidacion" },
  },
  {
    path: "/liquidacion/anularLiquidacion",
    name: "anularLiquidacion",
    component: anularLiquidacion,
    meta: { name: "Anulación" },
  },
  {
    path: "/liquidacion/habilitarcerrar",
    name: "habilitarCerrar",
    component: HabilitarCerrar,
    meta: { name: "Habilitar Cerrar" },
  },
  {
    path: "/liquidacion/consultaLiquidacion",
    name: "consultaLiquidacion",
    component: ConsultaLiquidacion,
    meta: { name: "Consulta Liquidación" },
  },
  {
    path: "/liquidacion/generarArchivoDeposito",
    name: "inicioGenerarArchivoDeposito",
    component: inicioGenerarArchivoDeposito,
    meta: { name: "Generar Archivo Deposito" },
  },
  {
    path: "/liquidacion/informeliquidacion",
    name: "inicioInformeLiquidacion",
    component: inicioInformeLiquidacion,
    meta: { name: "Informe Liquidación" },
  },
  {
    path: "/InicioArchivoSafin",
    name: "inicioArchivoSafin",
    component: inicioArchivoSafin,
    meta: { name: "Generar Archivo SAFIN" },
  },
  {
    path: "/InicioArchivoSafin/generarArchivoSafin",
    name: "generar-archivo-safin",
    component: InicioGenerarArchivoSafin,
    meta: { name: "Generar Archivo" },
  },
  {
    path: "/inicioLecturaSafin",
    name: "inicioLecturaSafin",
    component: inicioLecturaSafin,
    meta: { name: "Lectura Archivo SANFIN" },
  },
  //bapro
  {
    path: "/inicioPedidosCuenta",
    name: "inicioPedidosCuenta",
    component: inicioPedidosCuenta,
    meta: { name: "Apertura De Cuentas" },
  },
  {
    path: "/inicioPedidosCuenta/generarArchivo",
    name: "genear-archivo",
    component: inicioPedidosArchivo,
    meta: { name: "Generar Archivo" },
  },
  {
    path: "/inicioLecturaCuentas",
    name: "inicioLecturaCuentas",
    component: inicioLecturaCuentas,
    meta: { name: "Lectura De Cuenta" },
  },
  {
    path: "/inicioConsultaCuentas",
    name: "inicioConsultaCuentas",
    component: InicioConsultaCuentas,
    meta: { name: "Consulta De Cuentas" },
  },

  // BECAS (ACTUALIZACIÓN Y VISADO)
  {
    path: "/becasActualizacion/:id_tabla_sol(\\d+)?",
    name: "becasActualizacion",
    component: BecasActualizacion,
    meta: { name: "Becas (actualización y visado)" },
  },
  {
    path: "/becasActualizacion/abm/:id_beca(\\d+)?/:id_tabla_sol(\\d+)?/:es_abm(\\d)?",
    name: "becasActualizacionAbm",
    component: BecasActualizacionAbm,
    meta: { name: "Datos de la beca" },
  },

  // SOLICITANTES (CONSULTA)
  {
    path: "/consSolicitantes",
    name: "consSolicitantes",
    component: ConsSolicitantes,
    meta: { name: "Solicitantes (consulta)" },
  },

  // TRANSFERENCIAS
  {
    path: "/transferencias/:id_tabla_sol(\\d+)?",
    name: "inicioTransferencias",
    component: inicioTransferencias,
    meta: { name: "Transferencias" },
  },
  {
    path: "/transferencias/editar/:periodo(\\d+)?/:id_tabla_sol(\\d+)?",
    name: "editar-transf",
    component: EdicionTransferencias,
    meta: { name: "Actualizacion" },
  },
  // BAJADA FORMULARIOS
  {
    path: "/bajadaFormularios",
    name: "bajadaFormularios",
    component: BajadaFormularios,
    meta: { name: "Bajada Formularios" },
  },

  //CONSULTA INFORME BECAS
  {
    path: "/becas/consulta",
    name: "consultaBecas",
    component: inicioInformeBecas,
    meta: { name: "Consulta" },
  },
  {
    path: "/inicioInformeBecasDetalle/:id_beca(\\d+)?/:id_tabla_sol(\\d+)?/:es_abm(\\d)?",
    name: "inicioInformeBecasDetalle",
    component: inicioInformeBecasDetalle,
    meta: { name: "Informes de Becas Detalle" },
  },

  //inhabilitar carga becas
  {
    path: "/inhabilitarBecas",
    name: "inhabilitarBecas",
    component: inhabilitarBecas,
    meta: { name: "Inhabilitar Carga Becas" },
  },
  //inhabilitar carga becas
  {
    path: "/habilitarPeriodo", //camino path
    name: "habilitarPeriodo", // es el path de intranet 
    component: Periodo, // componenete vista
    meta: { name: "Habilitar Período" }, // nombre de la opción de intranet
  },
  //informe liquidación tesorería 
  {
    path: "/inicioInformeTesoreria", //camino path
    name: "inicioInformeTesoreria", // es el path de intranet 
    component: InformeTesoreria, // componenete vista
    meta: { name: "Informes Tesoreria" }, // nombre de la opción de intranet
  },

];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
