import { defineStore } from "pinia";
import http from "@/http";
import type { AxiosResponse } from "axios";
import { urlApi } from "@/shared.js";
// Entities
import Safin from "@/entities/Liquidaciones/Safin/Cabecerasafin"

export const useCabeceraSafinStore = defineStore("safin", {
  state: () => ({ listadoSafin: [] as Safin[], }),
  actions: {
    async updateListadoCabeceraSafin(params?: {
      f_desde: string,
      f_hasta: string,
    }) {
      const paramsRequest: any = {};
      paramsRequest.f_desde = params?.f_desde;
      paramsRequest.f_hasta = params?.f_hasta;
      const response: AxiosResponse<Safin[]> = await http.request<Safin[]>({
        method: "GET",
        url: urlApi + "getCabecerasSafin",
        params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const cabecera: Safin[] = JSON.parse(data);
            const dataAux: Safin[] = [];
            cabecera.forEach((element: Safin) => {
              dataAux.push(Object.assign(new Safin(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoSafin = response.data;
    },
  },
});
