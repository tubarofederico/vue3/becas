import { defineStore } from "pinia";
import http from "@/http";
import type { AxiosResponse } from "axios";
import  { urlApi } from "@/shared.js";
// Entities
import Liquidacion  from "@/entities/Liquidaciones/Liquidacion";


export const useLiquidacionesStore = defineStore("Liquidaciones", {
  state: () => ({ listadoLiquidaciones: [] as Liquidacion[] ,licActual : Liquidacion  }),
  actions: {
    // grilla de liquidaciones 
    async updateListadoLiquidaciones(params?: {

    }) {
        const response: AxiosResponse<Liquidacion[]> = await http.request<Liquidacion[]>({
        method: "GET",
        url: urlApi + "getLiqResulListar",
        transformResponse: [
          (data: string) => {
           
            const liquidaciones: Liquidacion[] = JSON.parse(data);
            const dataAux: Liquidacion[] = [];
            liquidaciones.forEach((element: Liquidacion) => {
              dataAux.push(Object.assign(new Liquidacion(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoLiquidaciones = response.data;
    },

    async getLiqActual(): Promise<Liquidacion | undefined> {
      
      //recupera del listado la ultima liquidación pero para el proceso de liq 

      const response: AxiosResponse<Liquidacion> = await http.request<Liquidacion>({
        method: "GET",
        url: urlApi + "getLiqResulActual",
        transformResponse: [
          (data: string) => {
               const liq: Liquidacion = JSON.parse(data);
               this.licActual =  Object.assign(new Liquidacion(), liq);
          },
        ],
      });
      return new Promise((resolve) => {
        resolve(this.licActual);
      });
    },

    async getLiqResulActual(): Promise<Liquidacion | undefined> {
      //recupera del listado la ultima liquidación 
      if (this.listadoLiquidaciones.length === 0) {
        await this.updateListadoLiquidaciones();
      }
      return new Promise((resolve) => {
        resolve(this.listadoLiquidaciones[0])
      });
    },


     async findUsuarioById(
      id_usuario?: number,
      force: boolean = false
      ): Promise<Liquidacion | undefined> {
      if (this.listadoLiquidaciones.length === 0 || force) {
        await this.updateListadoLiquidaciones();
      }
      const liq = this.listadoLiquidaciones.find(
        (liq: Liquidacion) => {
          return liq.id_liq_resu == null;
        }
      );
      return new Promise((resolve) => {
        resolve(liq);
      });
    }, 


  },
});
