import { defineStore } from "pinia";
import http from "@/http";
import type { AxiosResponse } from "axios";
import  { urlApi } from "@/shared.js";
// Entities
import CabeceraBecas  from "@/entities/Liquidaciones/CabeceraBecas";


export const useLiqBecasCabeceraStore = defineStore("LiqBecasCabecera", {
  state: () => ({ listadoCabeceraBecas: [] as CabeceraBecas[] }),
  actions: {
    

    async updateListadoLiqCabeceraBecas(params?: {
        id_liq:Number,
    }) {
        const paramsRequest: any = {};
        paramsRequest.id_liq_resu = params?.id_liq;
        const response: AxiosResponse<CabeceraBecas[]> = await http.request<CabeceraBecas[]>({
        method: "GET",
        url: urlApi + "listadoLiquidacionBecas",
        params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const liquidaciones: CabeceraBecas[] = JSON.parse(data);
            const dataAux: CabeceraBecas[] = [];
            liquidaciones.forEach((element: CabeceraBecas) => {
              dataAux.push(Object.assign(new CabeceraBecas(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoCabeceraBecas = response.data;
    },



    
    async getListadoConsultaLiquidacion(params?: {
      //id_liq:Number,
  }) {
      const paramsRequest: any = {};
      //paramsRequest = params;
      const response: AxiosResponse<CabeceraBecas[]> = await http.request<CabeceraBecas[]>({
      method: "GET",
      url: urlApi + "getListadoConsultaLiquidacion",
      params: params,
      transformResponse: [
        (data: string) => {
          const liquidaciones: CabeceraBecas[] = JSON.parse(data);
          const dataAux: CabeceraBecas[] = [];
          liquidaciones.forEach((element: CabeceraBecas) => {
            dataAux.push(Object.assign(new CabeceraBecas(), element));
          });
          return dataAux;
        },
      ],
    });
    this.listadoCabeceraBecas = response.data;
  },




   

  },
});
