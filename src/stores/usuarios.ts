import { defineStore } from "pinia";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { SweetAlertOptions } from "sweetalert2";
import type { AxiosResponse } from "axios";
import { urlApi, urlSeguridad, isEmpty } from "@/shared.js";
// Entities
import Solicitante from "@/entities/Solicitante";
import Usuario from "@/entities/Usuario";
import Bloque from "@/entities/Bloque";
import { Usuariohabilitado } from "@/entities/Usuariohabilitado";
import { Perfilactualiza } from "@/entities/Perfilactualiza";
import Usuariosolicitantes from "@/entities/Usuariosolicitantes";
import Bloquesdiputados from "@/entities/Bloquesdiputados";


export const useUsuariosStore = defineStore("usuarios", {
  state: () => ({ listadoSolicitantes: [] as Solicitante[], listadoUsuarios: [] as Usuario[], listadoBloques: [] as Bloque[], listadoUsuariosHabilitados: [] as Usuariohabilitado[], listadoPerfilActualiza: [] as Perfilactualiza[], listadoUsuariosSolicitantes: [] as Usuariosolicitantes[], listadoUsuariosBloquesDiputados: [] as Bloquesdiputados[] }),
  actions: {
    //Autocompletar de Solicitantes
    async updateListadoSolicitantes(
      estado: string | null = null
    ) {

      const response: AxiosResponse<Solicitante[]> = await http.request<Solicitante[]>({
        method: "GET",
        url: urlApi + "getSolicitantes",
        params: {
          estado: estado
        },
        transformResponse: [
          (data: string) => {
            const solicitantes: Solicitante[] = JSON.parse(data);
            const dataAux: Solicitante[] = [];
            solicitantes.forEach((element: Solicitante) => {
              dataAux.push(Object.assign(new Solicitante(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoSolicitantes = response.data;
    },
    //Autocompletar de Usuarios
    async updateListadoUsuarios(params?: {
      id_app: string,
    }) {
      if (this.listadoUsuarios.length == 0) {
        const paramsRequest: any = {};
        //paramsRequest.id_app = params?.id_app;
        paramsRequest.id_app = params;
        const response: AxiosResponse<Usuario[]> = await http.request<Usuario[]>({
          method: "GET",
          //url: urlApi + "getUsuariosAppActivos",
          //url: "http://localhost/admin/seguridad/getUsuariosAppActivos",
          url: urlSeguridad + "getUsuariosAppActivos",
          params: paramsRequest,
          transformResponse: [
            (data: string) => {
              const usuarios: Usuario[] = JSON.parse(data);
              const dataAux: Usuario[] = [];
              usuarios.res.forEach((element: Usuario) => {
                dataAux.push(Object.assign(new Usuario(), element));
              });
              return dataAux;
            },
          ],
        });
        this.listadoUsuarios = response.data;
      }
    },
    //Autocompletar de Bloques
    async updateListadoBloques(params?: {

    }) {

      const response: AxiosResponse<Bloque[]> = await http.request<Bloque[]>({
        method: "GET",
        url: urlApi + "getBloquesPoliticos",
        //params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const bloque: Bloque[] = JSON.parse(data);
            const dataAux: Bloque[] = [];
            bloque.forEach((element: Bloque) => {
              dataAux.push(Object.assign(new Bloque(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoBloques = response.data;
    },
    //Select Perfil Actualiza
    async updateListadoPerfilActualiza(params?: {
      perfil_actualiza: number;
    }) {
      const paramsRequest: any = {};
      paramsRequest.perfil_actualiza = params?.perfil_actualiza;

      const response: AxiosResponse<Perfilactualiza[]> = await http.request<Perfilactualiza[]>({
        method: "GET",
        url: urlApi + "getPerfiles",
        params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const perfilactualiza: Perfilactualiza[] = JSON.parse(data);
            const dataAux: Perfilactualiza[] = [];
            perfilactualiza.res.forEach((element: Perfilactualiza) => {
              dataAux.push(Object.assign(new Perfilactualiza(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoPerfilActualiza = response.data;
    },
    //Listado de Usuarios Habilitados Becas
    async updateListadoUsuariosHabilitados(params?: {
      solicitante?: number;
      usuario?: string;
      bloque?: number;
    }) {
      const paramsRequest: any = {};
      if (params !== undefined) {
        if (params.solicitante !== undefined) {
          paramsRequest.solicitante = params.solicitante;
        }

        if (
          params.usuario !== undefined
        ) {
          paramsRequest.usuario = params.usuario.trim();
        }

        if (
          params.bloque !== undefined
        ) {
          paramsRequest.bloque = params.bloque;
        }
      }

      const response: AxiosResponse<Usuariohabilitado[]> = await http.request<Usuariohabilitado[]>({
        method: "GET",
        url: urlApi + "getUsuariosHabilitadosBecas",
        params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const usuariohabilitado: Usuariohabilitado[] = JSON.parse(data);
            const dataAux: Usuariohabilitado[] = [];
            usuariohabilitado.res.forEach((element: Usuariohabilitado) => {
              dataAux.push(Object.assign(new Usuariohabilitado(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoUsuariosHabilitados = response.data;
    },

    //buscar por id_usuario_perfil (number) en usuarios Habilitados (sobre toda la lista)
    async findUsuarioById(
      id_usuario_perfil?: number,
      force: boolean = false
    ): Promise<Usuariohabilitado | undefined> {
      if (this.listadoUsuariosHabilitados.length === 0 || force) {
        await this.updateListadoUsuariosHabilitados();
      }
      const usuarioHabilitado = this.listadoUsuariosHabilitados.find(
        (usuarioHabilitado: Usuariohabilitado) => {
          return usuarioHabilitado.id_usuario_perfil == id_usuario_perfil;
        }
      );
      return new Promise((resolve) => {
        resolve(usuarioHabilitado);
      });
    },

    //buscar por id_usuario (string) en usuarios Habilitados (sobre toda la lista)
    async findUsuarioByIdUsuario(id_usuario: string, forceUpdate: boolean = false): Promise<Usuariohabilitado | null> {
      const swal: SweetAlertOptions = {
        icon: "error",
        title: "Error",
        html: ""
      };
      //let goFind: boolean = true;
      let usuarioHabilitado: Usuariohabilitado | undefined = undefined;
      if (isEmpty(this.listadoUsuariosHabilitados) || forceUpdate) {
        //goFind = await this.updateListadoUsuariosHabilitados();
        await this.updateListadoUsuariosHabilitados();
      }
      //if (goFind) {
      usuarioHabilitado = this.listadoUsuariosHabilitados.find((usuarioHabilitado: Usuariohabilitado) => {
        return usuarioHabilitado.id_usuario === id_usuario;
      });
      if (isEmpty(usuarioHabilitado)) {
        swal.html =
          "No se encontr&oacute; en el sistema el usuario habilitado buscado. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
      //}
      return new Promise((resolve) => {
        resolve((typeof usuarioHabilitado !== "undefined") ? usuarioHabilitado : null);
      });
    },

    //Listado de Solicitantes asociados Usuario -> listadoUsuariosSolicitantes
    async updateListadoUsuariosSolicitantes(params?: {
      id_usuario_perfil?: number;
    }) {
      const paramsRequest: any = {};
      if (params !== undefined) {
        if (params.id_usuario_perfil !== undefined) {
          paramsRequest.id_usuario_perfil = params.id_usuario_perfil;
        }
      }
      const response: AxiosResponse<Usuariosolicitantes[]> = await http.request<Usuariosolicitantes[]>({
        method: "GET",
        url: urlApi + "getUsuarioSolicitantesBecas",
        params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const usuariossolicitantes: Usuariosolicitantes[] = JSON.parse(data);
            const dataAux: Usuariosolicitantes[] = [];
            usuariossolicitantes.res.forEach((element: Usuariosolicitantes) => {
              dataAux.push(Object.assign(new Usuariosolicitantes(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoUsuariosSolicitantes = response.data;
    },

    //Listado Solicitantes asiciados a un Bloque -> listadoUsuariosBloque
    async updateListadoUsuariosBloquesDiputados(params?: {
      id_dependen: number;
      id_usuario_perfil: number;
    }) {
      const paramsRequest: any = {};
      if (params !== undefined) {
        if (params.id_dependen !== undefined) {
          paramsRequest.id_dependen = params.id_dependen;
        }

        if (
          params.id_usuario_perfil !== undefined
        ) {
          paramsRequest.id_usuario_perfil = params.id_usuario_perfil;
        }
      }

      const response: AxiosResponse<Bloquesdiputados[]> = await http.request<Bloquesdiputados[]>({
        method: "GET",
        url: urlApi + "getBloquesUsuariosDiputados",
        params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const bloquesdiputados: Bloquesdiputados[] = JSON.parse(data);
            const dataAux: Bloquesdiputados[] = [];
            bloquesdiputados.res.forEach((element: Bloquesdiputados) => {
              dataAux.push(Object.assign(new Bloquesdiputados(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoUsuariosBloquesDiputados = response.data;
    },

    //buscar por id el solicitante (sobre toda la lista)
    async findSolicitanteById(
      id_tabla_sol?: number,
      force: boolean = false
    ): Promise<Solicitante | undefined> {
      if (this.listadoSolicitantes.length === 0 || force) {
        await this.updateListadoSolicitantes();
      }
      const solicitante = this.listadoSolicitantes.find(
        (solicitante: Solicitante) => {
          return solicitante.id_tabla_sol == id_tabla_sol;
        }
      );
      return new Promise((resolve) => {
        resolve(solicitante);
      });
    },

  },
});
