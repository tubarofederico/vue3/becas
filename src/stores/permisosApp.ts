import { defineStore } from "pinia";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { urlComun, isEmpty, checkParams } from "@/shared.js";

// Entities
import { Option } from "@hcd-bsas/vue3-components";
import UsuarioSesion from "@/entities/Comun/UsuarioSesion";

export const usePermisosAppStore = defineStore("permisosApp", {
	state: () => ({
		listadoOp: [] as Option[],
		currentOp: null as Option | null,
		currentUser: null as UsuarioSesion | null
	}),
	actions: {

		async updateListado(paramsRequest?: Object): Promise<boolean> {
			const allParamsNames: String[] = ["id_app"];
			const swal = {
				icon: "error",
				title: "Error",
				html: "",
			} as SweetAlertOptions;
			let exito: boolean = true;
			let listadoOp: Option[] = [];
			// clearVisadosHist();
			this.listadoOp = [];
			if (!checkParams(paramsRequest!, allParamsNames)) {
				swal.html = "Los par&aacute;metros enviados para la obtenci&oacute;n del listado de opciones del men&uacute; son incorrectos. Por favor, avise al Administrador.";
				Swal.fire(swal);
				return false;
			}
			await http.get<Option[]>(urlComun + "getMenu", { params: paramsRequest })
				.then(function (response: AxiosResponse<Option[]>) {
					listadoOp = response.data;
					/*
					response.data.forEach((opcion: Option) => {
						listadoOp.push(Object.assign(new Option(), opcion));
					});
					*/
				})
				.catch(function (error: AxiosError<string>) {
					if (error.response) {
						// Request made and server responded
						swal.html = error.response.data;
						console.log("Error: request made and server responded");
						console.log(swal.html);
						console.log(error.response);
						//console.log(error.response.status);
						//console.log(error.response.headers);
						//console.log(error.response.data);
					} else if (error.request) {
						// The request was made but no response was received
						swal.html = "Error desconocido. Por favor, avise al Administrador.";
						console.log("Error: the request was made but no response was received");
						console.log(error.request);
					} else {
						// Something happened in setting up the request that triggered an Error
						swal.html = "Error desconocido. Por favor, avise al Administrador.";
						console.log("Error: something happened in setting up the request that triggered an error");
						console.log(error);
					}
					Swal.fire(swal);
					exito = false;
				});
			this.listadoOp = listadoOp;
			return exito;
		},

		async updateUser() {
			const swal = {
				icon: "error",
				title: "Error",
				html: "",
			} as SweetAlertOptions;
			let exito: boolean = true;
			let usuarioSesion: UsuarioSesion | null = null;
			this.currentUser = null;
			await http.get<UsuarioSesion>(urlComun + "getUsuarioSesion")
				.then(function (response: AxiosResponse<UsuarioSesion>) {
					//usuarioSesion = response.data;
					usuarioSesion = Object.assign(new UsuarioSesion(), response.data);
				})
				.catch(function (error: AxiosError<string>) {
					if (error.response) {
						// Request made and server responded
						swal.html =
							error.message +
							" (" +
							error.code +
							")." +
							"<br />" +
							error.response.data.substring(
								error.response.data.indexOf("<h2>") + 4,
								error.response.data.indexOf("</h2>")
							);
						console.log("Error: request made and server responded");
						console.log(swal.html);
						console.log(error.response);
						//console.log(error.response.status);
						//console.log(error.response.headers);
						//console.log(error.response.data);
					} else if (error.request) {
						// The request was made but no response was received
						swal.html = "Error desconocido. Por favor, avise al Administrador.";
						console.log(
							"Error: the request was made but no response was received"
						);
						console.log(error.request);
					} else {
						// Something happened in setting up the request that triggered an Error
						swal.html = "Error desconocido. Por favor, avise al Administrador.";
						console.log(
							"Error: something happened in setting up the request that triggered an error"
						);
						console.log(error);
					}
					Swal.fire(swal);
					exito = false;
				});
			this.currentUser = usuarioSesion;
			return exito;
		},

		async findOption(
			path: string,
			forceUpdate: boolean = false,
			paramsUpdate?: { id_app: string; }
		): Promise<Option | null> {
			const swal: SweetAlertOptions = {
				icon: "error",
				title: "Error",
				html: ""
			};
			const findMatch = (options: Option[]) => {
				return options.find((option: Option) => {
					if (option.route instanceof Array) {
						findMatch(option.route);
					} else {
						//return option.data.url === path;
						return option.route === path;
					}
				});
			};
			let option: Option | undefined = undefined;
			if (isEmpty(this.listadoOp) || forceUpdate) {
				await this.updateListado(paramsUpdate);
			}
			option = findMatch(this.listadoOp);
			if (isEmpty(option)) {
				swal.html =
					"No se encontr&oacute; en el sistema la opci&oacute;n del men&uacute; buscada. Por favor, avise al Administrador.";
				Swal.fire(swal);
			}
			return new Promise((resolve) => {
				resolve((typeof option !== "undefined") ? option : null);
			});
		},

		async updateCurrentOption(path: string) {
			this.currentOp = await this.findOption(path);
		}

	}
});





