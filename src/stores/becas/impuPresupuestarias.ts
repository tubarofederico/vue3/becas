import { defineStore } from "pinia";
import { ref } from "vue";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { checkParams, urlComun, isEmpty } from "@/shared.js";

// Entities
import ImpuPresupuestaria from "@/entities/becas/ImpuPresupuestaria";

export const useImpuPresupuestariasStore = defineStore("impuPresupuestarias", () => {

  const impuPresupuestarias = ref([] as ImpuPresupuestaria[]);

  function clearImpuPresupuestarias() {
    impuPresupuestarias.value = [];
  }

  function getImpuPresupuestarias(): ImpuPresupuestaria[] {
    return impuPresupuestarias.value;
  }

  async function fetchImpuPresupuestarias(paramsRequest: Object = {}): Promise<boolean> {
    const allParamsNames: String[] = ["id_impu_presupuestaria", "c_impu_presupuestaria"];
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let isSuccess: boolean = true;
    clearImpuPresupuestarias();
    if (!checkParams(paramsRequest, allParamsNames)) {
      swal.html =
        "Los par&aacute;metros enviados para la obtenci&oacute;n del listado de las imputaciones presupuestarias son incorrectos. Por favor, avise al Administrador.";
      Swal.fire(swal);
      return false;
    }
    await http.get<ImpuPresupuestaria[]>(urlComun + "getImpuPresupuestarias", { params: paramsRequest })
      .then(function (response: AxiosResponse<ImpuPresupuestaria[]>) {
        impuPresupuestarias.value = response.data;
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          if (error.response.data.indexOf("|") !== -1) {
            swal.html = error.response.data.substring(
              error.response.data.indexOf("|") + 2,
              error.response.data.indexOf("Código") - 8
            );
          } else {
            swal.html =
              error.response.data.substring(
                error.response.data.indexOf("<h2>") + ("<h2>").length,
                error.response.data.indexOf("</h2>")
              );
          }
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          //console.log(error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html =
            "Error desconocido. Por favor, avise al Administrador.";
          console.log("Error: the request was made but no response was received");
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html =
            "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: something happened in setting up the request that triggered an error"
          );
          console.log(error);
        }
        Swal.fire(swal);
        isSuccess = false;
      });
    return isSuccess;
  };

  async function findImpuPresupuestaria(c_impu_presupuestaria: string, forceUpdate: boolean = false, paramsUpdate: Object = {}): Promise<ImpuPresupuestaria | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let impuPresupuestaria: ImpuPresupuestaria | undefined = undefined;
    if (isEmpty(getImpuPresupuestarias()) || forceUpdate) {
      goFind = await fetchImpuPresupuestarias(paramsUpdate);
    }
    if (goFind) {
      impuPresupuestaria = getImpuPresupuestarias().find((impuPresupuestaria: ImpuPresupuestaria) => {
        return impuPresupuestaria.c_impu_presupuestaria === c_impu_presupuestaria;
      });
      if (isEmpty(impuPresupuestaria)) {
        swal.html =
          "No se encontr&oacute; en el sistema la imputaci&oacute;n presupuestaria buscada. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
    }
    return new Promise((resolve) => {
      resolve((typeof impuPresupuestaria !== "undefined") ? impuPresupuestaria : null);
    });
  };

  return { clearImpuPresupuestarias, getImpuPresupuestarias, fetchImpuPresupuestarias, findImpuPresupuestaria };

});
