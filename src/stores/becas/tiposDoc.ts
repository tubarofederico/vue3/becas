import { defineStore } from "pinia";
import { ref } from "vue";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { urlComun, isEmpty } from "@/shared.js";

// Entities
import TipoDoc from "@/entities/becas/TipoDoc";

export const useTiposDocStore = defineStore("tiposDoc", () => {

  const tiposDoc = ref([] as TipoDoc[]);

  function clearTiposDoc() {
    tiposDoc.value = [];
  }

  function getTiposDoc(): TipoDoc[] {
    return tiposDoc.value;
  }

  async function fetchTiposDoc(): Promise<boolean> {
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let exito: boolean = true;
    clearTiposDoc();
    await http.get<TipoDoc[]>(urlComun + "getTiposDoc")
      .then(function (response: AxiosResponse<TipoDoc[]>) {
        tiposDoc.value = response.data;
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          swal.html =
            error.message +
            " (" +
            error.code +
            ")." +
            "<br />" +
            error.response.data.substring(
              error.response.data.indexOf("<h2>") + 4,
              error.response.data.indexOf("</h2>")
            );
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          //console.log(error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: the request was made but no response was received"
          );
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: something happened in setting up the request that triggered an error"
          );
          console.log(error);
        }
        Swal.fire(swal);
        exito = false;
      });
    return exito;
  };

  async function findTipoDoc(id_tipo_doc: number, forceUpdate: boolean = false): Promise<TipoDoc | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let tipoDoc: TipoDoc | undefined = undefined;
    if (isEmpty(tiposDoc.value) || forceUpdate) {
      goFind = await fetchTiposDoc();
    }
    if (goFind) {
      tipoDoc = tiposDoc.value.find((tipoDoc: TipoDoc) => {
        return tipoDoc.id_tipo_doc === id_tipo_doc;
      });
      if (isEmpty(tipoDoc)) {
        swal.html =
          "No se encontr&oacute; en el sistema el tipo de documento buscado. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
    }
    return new Promise((resolve) => {
      resolve((typeof tipoDoc !== "undefined") ? tipoDoc : null);
    });
  };

  return { clearTiposDoc, getTiposDoc, fetchTiposDoc, findTipoDoc };

});
