import { defineStore } from "pinia";
import { ref } from "vue";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { urlApi, isEmpty, checkParams } from "@/shared.js";

// Entities
import LiqDetalle from "@/entities/becas/LiqDetalle";

export const useLiqDetallesStore = defineStore("liqDetalles", () => {

  const liqDetalles = ref([] as LiqDetalle[]);

  function clearLiqDetalles() {
    liqDetalles.value = [];
  }

  function getLiqDetalles(): LiqDetalle[] {
    return liqDetalles.value;
  }

  async function fetchLiqDetalles(paramsRequest: Object): Promise<boolean> {
    const allParamsNames: String[] = ["id_beca"];
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let exito: boolean = true;
    clearLiqDetalles();
    if (!checkParams(paramsRequest, allParamsNames)) {
      swal.html = "Los par&aacute;metros enviados para la obtenci&oacute;n del listado de detalles de liquidaci&oacute;n son incorrectos. Por favor, avise al Administrador.";
      Swal.fire(swal);
      return false;
    }
    await http.get<LiqDetalle[]>(urlApi + "getLiqDetalles", { params: paramsRequest })
      .then(function (response: AxiosResponse<LiqDetalle[]>) {
        liqDetalles.value = response.data;
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          swal.html = error.response.data;
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log("Error: the request was made but no response was received");
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log("Error: something happened in setting up the request that triggered an error");
          console.log(error);
        }
        Swal.fire(swal);
        exito = false;
      });
    return exito;
  };

  async function findLiqDetalle(id_liq_deta: number, paramsUpdate: Object = {}): Promise<LiqDetalle | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let liqDetalle: LiqDetalle | undefined = undefined;
    if (isEmpty(liqDetalles.value) || !isEmpty(paramsUpdate)) { // "paramsUpdate" con datos/valores cargados implica "forceUpdate" en true
      goFind = await fetchLiqDetalles(paramsUpdate);
    }
    if (goFind) {
      liqDetalle = liqDetalles.value.find((liqDetalle: LiqDetalle) => {
        return liqDetalle.id_liq_deta === id_liq_deta;
      });
      if (isEmpty(liqDetalle)) {
        swal.html =
          "No se encontr&oacute; en el sistema el detalle de liquidaci&oacute;n buscado. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
    }
    return new Promise((resolve) => {
      resolve((typeof liqDetalle !== "undefined") ? liqDetalle : null);
    });
  };

  return { clearLiqDetalles, getLiqDetalles, fetchLiqDetalles, findLiqDetalle };

});
