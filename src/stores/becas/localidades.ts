import { defineStore } from "pinia";
import { ref } from "vue";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { urlComun, isEmpty } from "@/shared.js";

// Entities
import Localidad from "@/entities/becas/Localidad";

export const useLocalidadesStore = defineStore("localidades", () => {

  const localidades = ref([] as Localidad[]);

  function clearLocalidades() {
    localidades.value = [];
  }

  function getLocalidades(): Localidad[] {
    return localidades.value;
  }

  async function fetchLocalidades(): Promise<boolean> {
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let exito: boolean = true;
    clearLocalidades();
    await http.get<Localidad[]>(urlComun + "getLocalidades")
      .then(function (response: AxiosResponse<Localidad[]>) {
        response.data.forEach((localidad: Localidad) => {
          localidades.value.push(Object.assign(new Localidad(), localidad)); // esta serie de instanciaciones es necesaria debido a que este listado se utiliza en un autocompletar
        });
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          swal.html =
            error.message +
            " (" +
            error.code +
            ")." +
            "<br />" +
            error.response.data.substring(
              error.response.data.indexOf("<h2>") + 4,
              error.response.data.indexOf("</h2>")
            );
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          //console.log(error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: the request was made but no response was received"
          );
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: something happened in setting up the request that triggered an error"
          );
          console.log(error);
        }
        Swal.fire(swal);
        exito = false;
      });
    return exito;
  };

  async function findLocalidad(id_localidad: number, forceUpdate: boolean = false): Promise<Localidad | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let localidad: Localidad | undefined = undefined;
    if (isEmpty(localidades.value) || forceUpdate) {
      goFind = await fetchLocalidades();
    }
    if (goFind) {
      localidad = localidades.value.find((localidad: Localidad) => {
        return localidad.id_localidad === id_localidad;
      });
      if (isEmpty(localidad)) {
        swal.html =
          "No se encontr&oacute; en el sistema la localidad territorial buscada. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
    }
    return new Promise((resolve) => {
      resolve((typeof localidad !== "undefined") ? localidad : null);
    });
  };

  async function filterLocalidades(id_partido: number, forceUpdate: boolean = false): Promise<Localidad[] | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFilter: boolean = true;
    let localidadesFiltered: Localidad[] = [];
    if (isEmpty(localidades.value) || forceUpdate) {
      goFilter = await fetchLocalidades();
    }
    if (goFilter) {
      localidadesFiltered = localidades.value.filter((localidad: Localidad) => {
        return localidad?.id_partido === id_partido;
      });
      if (isEmpty(localidadesFiltered)) {
        swal.html =
          "No se encontr&oacute; en el sistema ninguna localidad para el partido territorial solicitado. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
    }
    return new Promise((resolve) => {
      resolve((!isEmpty(localidadesFiltered)) ? localidadesFiltered : null);
    });
  };

  return { clearLocalidades, getLocalidades, fetchLocalidades, findLocalidad, filterLocalidades };

});
