import { defineStore } from "pinia";
import { ref } from "vue";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { urlApi, isEmpty, checkParams } from "@/shared.js";

// Entities
import VisadoHist from "@/entities/becas/VisadoHist";

export const useVisadosHistStore = defineStore("visadosHist", () => {

  const visadosHist = ref([] as VisadoHist[]);

  function clearVisadosHist() {
    visadosHist.value = [];
  }

  function getVisadosHist(): VisadoHist[] {
    return visadosHist.value;
  }

  async function fetchVisadosHist(paramsRequest: Object): Promise<boolean> {
    const allParamsNames: String[] = ["id_beca"];
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let exito: boolean = true;
    clearVisadosHist();
    if (!checkParams(paramsRequest, allParamsNames)) {
      swal.html = "Los par&aacute;metros enviados para la obtenci&oacute;n del listado de visados hist&oacute;ricos son incorrectos. Por favor, avise al Administrador.";
      Swal.fire(swal);
      return false;
    }
    await http.get<VisadoHist[]>(urlApi + "getVisadosHist", { params: paramsRequest })
      .then(function (response: AxiosResponse<VisadoHist[]>) {
        visadosHist.value = response.data;
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          swal.html = error.response.data;
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          //console.log(error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log("Error: the request was made but no response was received");
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log("Error: something happened in setting up the request that triggered an error");
          console.log(error);
        }
        Swal.fire(swal);
        exito = false;
      });
    return exito;
  };

  async function findVisadoHist(id_beca_historico: number, paramsUpdate: Object = {}): Promise<VisadoHist | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let visadoHist: VisadoHist | undefined = undefined;
    if (isEmpty(visadosHist.value) || !isEmpty(paramsUpdate)) { // "paramsUpdate" con datos/valores cargados implica "forceUpdate" en true
      goFind = await fetchVisadosHist(paramsUpdate);
    }
    if (goFind) {
      visadoHist = visadosHist.value.find((visadoHist: VisadoHist) => {
        return visadoHist.id_beca_historico === id_beca_historico;
      });
      if (isEmpty(visadoHist)) {
        swal.html =
          "No se encontr&oacute; en el sistema el visado hist&oacute;rico buscado. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
    }
    return new Promise((resolve) => {
      resolve((typeof visadoHist !== "undefined") ? visadoHist : null);
    });
  };

  return { clearVisadosHist, getVisadosHist, fetchVisadosHist, findVisadoHist };

});
