// Librerías
import { defineStore } from "pinia";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { checkParams, urlApi, isEmpty } from "@/shared.js";
// Entidades
import Becario from "@/entities/becas/Becario";

export const useBecariosStore = defineStore("becarios", {
  state: () => ({
    becarios: [] as Becario[]
  }),

  actions: {
    async fetchBecarios(): Promise<boolean> {
      const swal = {
        icon: "error",
        title: "Error",
        html: "",
      } as SweetAlertOptions;
      let isSuccess: boolean = true;
      let becarios: Becario[] = [];
      this.becarios = [];
      await http.get<Becario[]>(urlApi + "getBecarios")
        .then(function (response: AxiosResponse<Becario[]>) {
          response.data.forEach((becario: Becario) => {
            becarios.push(Object.assign(new Becario(), becario)); // esta serie de instanciaciones es necesaria debido a que este listado se utiliza en un autocompletar
          });
        })
        .catch(function (error: AxiosError<string>) {
          if (error.response) {
            swal.html = error.response.data;
            console.log("Error: request made and server responded");
            console.log(swal.html);
            console.log(error.response);
            //console.log(error.response.status);
            //console.log(error.response.headers);
            //console.log(error.response.data);
          } else if (error.request) {
            // The request was made but no response was received
            swal.html = "Error desconocido. Por favor, avise al Administrador.";
            console.log(
              "Error: the request was made but no response was received"
            );
            console.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            swal.html = "Error desconocido. Por favor, avise al Administrador.";
            console.log(
              "Error: something happened in setting up the request that triggered an error"
            );
            console.log(error);
          }
          Swal.fire(swal);
          isSuccess = false;
        });
      this.becarios = becarios;
      return isSuccess;
    },

    async fetchBecario(paramsRequest: Object = {}, esBecario: boolean = true): Promise<Object> {
      const allParamsNames: String[] = ["id_becario", "tyn_doc", "id_beca"];
      const swal = {
        icon: "error",
        title: "Error",
        html: "",
      } as SweetAlertOptions;
      let isSuccess: boolean = true;
      let becario: Becario | undefined = undefined;
      if (!checkParams(paramsRequest, allParamsNames)) {
        swal.html = "Los par&aacute;metros enviados para la b&uacute;squeda y obtenci&oacute;n del " +
          (esBecario ? "becario" : "apoderado") + " son incorrectos. Por favor, avise al Administrador.";
        Swal.fire(swal);
        return { becario: becario, isSuccess: false };
      }
      await http.get<Becario>(urlApi + "getBecario", { params: paramsRequest })
        .then(function (response: AxiosResponse<Becario>) {
          becario = response.data;
        })
        .catch(function (error: AxiosError<string>) {
          if (error.response) {
            swal.html = error.response.data;
            console.log("Error: request made and server responded");
            console.log(swal.html);
            console.log(error.response);
            //console.log(error.response.status);
            //console.log(error.response.headers);
            //console.log(error.response.data);
          } else if (error.request) {
            // The request was made but no response was received
            swal.html = "Error desconocido. Por favor, avise al Administrador.";
            console.log(
              "Error: the request was made but no response was received"
            );
            console.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            swal.html = "Error desconocido. Por favor, avise al Administrador.";
            console.log(
              "Error: something happened in setting up the request that triggered an error"
            );
            console.log(error);
          }
          Swal.fire(swal);
          isSuccess = false;
        });
      if (!isEmpty(becario)) {
        if (!isEmpty(becario!.msj_validacion)) {
          swal.html = becario!.msj_validacion;
          Swal.fire(swal);
          isSuccess = false;
        } else if (!isEmpty(becario!.id_becario) && !isEmpty(this.becarios)) {
          isSuccess = this.abmBecario(becario!, "M");
        }
      }
      return { becario: becario, isSuccess: isSuccess };
    },

    async findBecarioById(
      paramsRequest: { id_becario: number, id_beca?: number },
      forceUpdate: boolean = false,
      esBecario: boolean = true
    ): Promise<Becario | null> {
      let goFind: { becario?: Becario, isSuccess?: boolean } = {};
      goFind.becario = (!forceUpdate) ?
        this.becarios.find((becario: Becario) => {
          return becario.id_becario === paramsRequest.id_becario &&
            becario.id_beca === paramsRequest.id_beca && typeof becario.msj_validacion === "string";
        }) :
        undefined;
      goFind = (typeof goFind.becario === "undefined") ?
        await this.fetchBecario(paramsRequest, esBecario) :
        { becario: goFind.becario, isSuccess: true };
      goFind.becario = ((goFind.isSuccess) && (typeof goFind.becario === "undefined")) ?
        this.becarios.find((becario: Becario) => {
          return becario.id_becario === paramsRequest.id_becario &&
            becario.id_beca === paramsRequest.id_beca && typeof becario.msj_validacion === "string";
        }) :
        goFind.becario;
      return new Promise((resolve) => {
        resolve((typeof goFind.becario !== "undefined") ? goFind.becario : null);
      });
    },

    async findBecarioByDoc(
      paramsRequest: { tyn_doc: string, id_beca?: number },
      forceUpdate: boolean = false,
      esBecario: boolean = true
    ): Promise<Becario | null> {
      const doc_becario = {
        tipo_doc: paramsRequest.tyn_doc.substring(0, 1),
        nro_doc: paramsRequest.tyn_doc.substring(1, 9)
      };
      let goFind: { becario?: Becario, isSuccess?: boolean } = {};
      goFind.becario = (!forceUpdate) ?
        this.becarios.find((becario: Becario) => {
          return becario.tipo_doc === doc_becario.tipo_doc && becario.doc === doc_becario.nro_doc &&
            becario.id_beca === paramsRequest.id_beca && typeof becario.msj_validacion === "string";
        }) :
        undefined;
      goFind = (typeof goFind.becario === "undefined") ?
        await this.fetchBecario(paramsRequest, esBecario) :
        { becario: goFind.becario, isSuccess: true };
      goFind.becario = ((goFind.isSuccess) && (typeof goFind.becario === "undefined")) ?
        this.becarios.find((becario: Becario) => {
          return becario.tipo_doc === doc_becario.tipo_doc && becario.doc === doc_becario.nro_doc &&
            becario.id_beca === paramsRequest.id_beca && typeof becario.msj_validacion === "string";
        }) :
        goFind.becario;
      return new Promise((resolve) => {
        resolve((typeof goFind.becario !== "undefined") ? goFind.becario : null);
      });
    },

    abmBecario(becario: Becario, accion: string): boolean {
      let indexBecario: number = -1;
      let isSuccess: boolean = true;
      if (accion === "A") {
        this.becarios.push(becario);
      } else {
        indexBecario = this.becarios.findIndex((becarioAux: Becario) => {
          return becarioAux.id_becario === becario.id_becario;
        });
        if (indexBecario !== -1) {
          if (accion === "M") {
            this.becarios[indexBecario] = becario;
          } else {
            isSuccess = (this.becarios.splice(indexBecario, 1).length === 1);
          }
        } else {
          isSuccess = false;
        }
      }
      if (isSuccess && accion !== "B" && accion !== "R") {
        this.becarios.sort((becarioA, becarioB) => (
          becarioA.apellido.concat(" ", becarioA.nombres).localeCompare(
            becarioB.apellido.concat(" ", becarioB.nombres)))
        );
      }
      return isSuccess;
    }
  }
});
