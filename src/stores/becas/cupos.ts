import { defineStore } from "pinia";
import { ref } from "vue";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { checkParams, urlApi, isEmpty } from "@/shared.js";

// Entities
import Cupo from "@/entities/becas/Cupo";

export const useCuposStore = defineStore("cupos", () => {

  const cupos = ref([] as Cupo[]);

  function clearCupos() {
    cupos.value = [];
  }

  async function fetchCupos(paramsRequest: Object): Promise<boolean> {
    const allParamsNames: String[] = ["f_cupo_des", "f_cupo_has", "id_tabla_sol", "usar_perfil_consulta"];
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let exito: boolean = true;
    clearCupos();
    if (!checkParams(paramsRequest, allParamsNames)) {
      swal.html =
        "Los par&aacute;metros enviados para la obtenci&oacute;n del listado de los cupos son incorrectos. Por favor, avise al Administrador.";
      Swal.fire(swal);
      return false;
    }
    await http.get<Cupo[]>(urlApi + "getCuposSolicitantes", { params: paramsRequest })
      .then(function (response: AxiosResponse<Cupo[]>) {
        cupos.value = response.data;
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          swal.html =
            error.message +
            " (" +
            error.code +
            ")." +
            "<br />" +
            error.response.data.substring(
              error.response.data.indexOf("<h2>") + 4,
              error.response.data.indexOf("</h2>")
            );
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          //console.log(error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: the request was made but no response was received"
          );
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: something happened in setting up the request that triggered an error"
          );
          console.log(error);
        }
        Swal.fire(swal);
        exito = false;
      });
    return exito;
  };

  async function findCupo(id_resumen_solicitante: number, forceUpdate: boolean = false, paramsUpdate: Object = {}): Promise<Cupo | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let cupo: Cupo | undefined = undefined;
    if (isEmpty(cupos.value) || forceUpdate) {
      goFind = await fetchCupos(paramsUpdate);
    }
    if (goFind) {
      cupo = cupos.value.find((cupo: Cupo) => {
        return cupo.id_resumen_solicitante === id_resumen_solicitante;
      });
      if (isEmpty(cupo)) {
        swal.html =
          "No se encontr&oacute; en el sistema el cupo buscado. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
    }
    return new Promise((resolve) => {
      resolve((typeof cupo !== "undefined") ? cupo : null);
    });
  };

  function getCupos() { // Retorna una copia del listado de cupos mapeados a Objetos "Cupo".
    let cuposAux: Cupo[] = [];
    cupos.value.forEach((cupo) => {
      cuposAux.push(Object.assign(new Cupo(), cupo));
    });
    return cuposAux;
  }

  return { cupos, clearCupos, fetchCupos, findCupo, getCupos };

});