import { defineStore } from "pinia";
import { ref } from "vue";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { checkParams, urlComun, isEmpty } from "@/shared.js";

// Entities
import Genero from "@/entities/becas/Genero";

export const useGenerosStore = defineStore("generos", () => {

  const generos = ref([] as Genero[]);

  function clearGeneros() {
    generos.value = [];
  }

  function getGeneros(): Genero[] {
    return generos.value;
  }

  async function fetchGeneros(paramsRequest: Object = {}): Promise<boolean> {
    const allParamsNames: String[] = ["id_genero"];
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let exito: boolean = true;
    clearGeneros();
    if (!checkParams(paramsRequest, allParamsNames)) {
      swal.html =
        "Los par&aacute;metros enviados para la obtenci&oacute;n del listado de los g&eacute;neros son incorrectos. Por favor, avise al Administrador.";
      Swal.fire(swal);
      return false;
    }
    await http.get<Genero[]>(urlComun + "getGeneros", { params: paramsRequest })
      .then(function (response: AxiosResponse<Genero[]>) {
        generos.value = response.data;
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          swal.html =
            error.message +
            " (" +
            error.code +
            ")." +
            "<br />" +
            error.response.data.substring(
              error.response.data.indexOf("<h2>") + 4,
              error.response.data.indexOf("</h2>")
            );
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          //console.log(error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: the request was made but no response was received"
          );
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: something happened in setting up the request that triggered an error"
          );
          console.log(error);
        }
        Swal.fire(swal);
        exito = false;
      });
    return exito;
  };

  async function findGenero(c_genero: string, forceUpdate: boolean = false, paramsUpdate: Object = {}): Promise<Genero | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let genero: Genero | undefined = undefined;
    if (isEmpty(getGeneros()) || forceUpdate) {
      goFind = await fetchGeneros(paramsUpdate);
    }
    if (goFind) {
      genero = getGeneros().find((genero: Genero) => {
        return genero.c_genero === c_genero;
      });
      if (isEmpty(genero)) {
        swal.html =
          "No se encontr&oacute; en el sistema el g&eacute;nero buscado. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
    }
    return new Promise((resolve) => {
      resolve((typeof genero !== "undefined") ? genero : null);
    });
  };

  return { clearGeneros, getGeneros, fetchGeneros, findGenero };

});
