import { defineStore } from "pinia";
import { ref } from "vue";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { checkParams, urlApi, isEmpty } from "@/shared.js";

// Entities
import Beca from "@/entities/becas/Beca";

export const useBecasStore = defineStore("becas", () => {

  const becas = ref([] as Beca[]);

  function clearBecas() {
    becas.value = [];
  }

  async function fetchBecas(paramsRequest: Object): Promise<boolean> {
    const allParamsNames: String[] = [
      "id_tabla_sol", "id_becario", "id_apoderado", "f_desde", "f_hasta",
      "id_expte", "id_tabla_sol_responsable", "id_dependen", "baprosuc",
      "id_beca", "estado", "usar_perfil_consulta"
    ];
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let exito: boolean = true;
    clearBecas();
    if (!checkParams(paramsRequest, allParamsNames)) {
      swal.html =
        "Los par&aacute;metros enviados para la obtenci&oacute;n del listado de las becas son incorrectos. Por favor, avise al Administrador.";
      Swal.fire(swal);
      return false;
    }
    await http.get<Beca[]>(urlApi + "getBecas", { params: paramsRequest })
      .then(function (response: AxiosResponse<Beca[]>) {
        becas.value = response.data;
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          swal.html = (error.response.data.indexOf("|") !== -1) ?
            error.response.data.substring(
              error.response.data.indexOf("|") + 2,
              error.response.data.indexOf("Código") - 8
            ) :
            error.response.data.substring(
              error.response.data.indexOf("<h2>") + ("<h2>").length,
              error.response.data.indexOf("</h2>")
            );
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          //console.log(error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: the request was made but no response was received"
          );
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: something happened in setting up the request that triggered an error"
          );
          console.log(error);
        }
        Swal.fire(swal);
        exito = false;
      });
    return exito;
  };

  async function informeBecasListar(paramsRequest: Object): Promise<boolean> {
    const allParamsNames: String[] = [
      "id_tabla_sol", "id_becario", "id_apoderado", "f_desde", "f_hasta",
      "id_expte", "id_tabla_sol_responsable", "id_dependen", "baprosuc",
      "id_beca", "estado", "usar_perfil_consulta","orden","cond_visado",
    ];
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let exito: boolean = true;
    clearBecas();
    /* if (!checkParams(paramsRequest, allParamsNames)) {
      swal.html =
        "Los par&aacute;metros enviados para la obtenci&oacute;n del listado de las becas son incorrectos. Por favor, avise al Administrador.";
      Swal.fire(swal);
      return false;
    } */
    await http.get<Beca[]>(urlApi + "informeBecasConsulta", { params: paramsRequest })
      .then(function (response: AxiosResponse<Beca[]>) {
        becas.value = response.data;
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          swal.html =
            error.message +
            " (" +
            error.code +
            ")." +
            "<br />" +
            error.response.data.substring(
              error.response.data.indexOf("<h2>") + 4,
              error.response.data.indexOf("</h2>")
            );
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          //console.log(error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: the request was made but no response was received"
          );
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: something happened in setting up the request that triggered an error"
          );
          console.log(error);
        }
        Swal.fire(swal);
        exito = false;
      });
    return exito;
  };

  async function findBeca(id_beca: number, forceUpdate: boolean = false, paramsUpdate: Object = {}): Promise<Beca | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let beca: Beca | undefined = undefined;
    if (isEmpty(becas.value) || forceUpdate) {
      goFind = await fetchBecas(paramsUpdate);
    }
    if (goFind) {
      beca = becas.value.find((beca: Beca) => {
        return beca.id_beca === id_beca;
      });
      if (isEmpty(beca)) {
        swal.html =
          "No se encontr&oacute; en el sistema la beca buscada. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
    }
    return new Promise((resolve) => {
      resolve((typeof beca !== "undefined") ? beca : null);
    });
  };

  return { becas, clearBecas, fetchBecas, findBeca, informeBecasListar };

});
