import { defineStore } from "pinia";
import { ref } from "vue";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { urlComun, isEmpty } from "@/shared.js";

// Entities
import Partido from "@/entities/becas/Partido";

export const usePartidosStore = defineStore("partidos", () => {

  const partidos = ref([] as Partido[]);

  function clearPartidos() {
    partidos.value = [];
  }

  function getPartidos(): Partido[] {
    return partidos.value;
  }

  async function fetchPartidos(): Promise<boolean> {
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let exito: boolean = true;
    clearPartidos();
    await http.get<Partido[]>(urlComun + "getPartidos")
      .then(function (response: AxiosResponse<Partido[]>) {
        response.data.forEach((partido: Partido) => {
          partidos.value.push(Object.assign(new Partido(), partido)); // esta serie de instanciaciones es necesaria debido a que este listado se utiliza en un autocompletar
        });
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          swal.html =
            error.message +
            " (" +
            error.code +
            ")." +
            "<br />" +
            error.response.data.substring(
              error.response.data.indexOf("<h2>") + 4,
              error.response.data.indexOf("</h2>")
            );
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          //console.log(error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: the request was made but no response was received"
          );
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: something happened in setting up the request that triggered an error"
          );
          console.log(error);
        }
        Swal.fire(swal);
        exito = false;
      });
    return exito;
  };

  async function findPartido(id_partido: number, forceUpdate: boolean = false): Promise<Partido | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let partido: Partido | undefined = undefined;
    if (isEmpty(partidos.value) || forceUpdate) {
      goFind = await fetchPartidos();
    }
    if (goFind) {
      partido = partidos.value.find((partido: Partido) => {
        return partido.id_partido === id_partido;
      });
      if (isEmpty(partido)) {
        swal.html =
          "No se encontr&oacute; en el sistema el partido territorial buscado. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
    }
    return new Promise((resolve) => {
      resolve((typeof partido !== "undefined") ? partido : null);
    });
  };

  return { clearPartidos, getPartidos, fetchPartidos, findPartido };

});
