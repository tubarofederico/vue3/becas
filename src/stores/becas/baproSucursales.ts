import { defineStore } from "pinia";
import { ref } from "vue";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { checkParams, urlComun, isEmpty } from "@/shared.js";

// Entities
import BaproSucursal from "@/entities/becas/BaproSucursal";

export const useBaproSucursalesStore = defineStore("baproSucursales", () => {

  const baproSucursales = ref([] as BaproSucursal[]);

  function clearBaproSucursales() {
    baproSucursales.value = [];
  }

  function getBaproSucursales(): BaproSucursal[] {
    return baproSucursales.value;
  }

  async function fetchBaproSucursales(paramsRequest: Object = {}): Promise<boolean> {
    const allParamsNames: String[] = ["orden"];
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let exito: boolean = true;
    clearBaproSucursales();
    if (!checkParams(paramsRequest, allParamsNames)) {
      swal.html =
        "Los par&aacute;metros enviados para la obtenci&oacute;n del listado de las sucursales Bapro son incorrectos. Por favor, avise al Administrador.";
      Swal.fire(swal);
      return false;
    }
    await http.get<BaproSucursal[]>(urlComun + "getBaproSucursales", { params: paramsRequest })
      .then(function (response: AxiosResponse<BaproSucursal[]>) {
        response.data.forEach((baproSucursal: BaproSucursal) => {
          baproSucursales.value.push(Object.assign(new BaproSucursal(), baproSucursal)); // esta serie de instanciaciones es necesaria debido a que este listado se utiliza en un autocompletar
        });
      })
      .catch(function (error: AxiosError<string>) {
        if (error.response) {
          // Request made and server responded
          swal.html =
            error.message +
            " (" +
            error.code +
            ")." +
            "<br />" +
            error.response.data.substring(
              error.response.data.indexOf("<h2>") + 4,
              error.response.data.indexOf("</h2>")
            );
          console.log("Error: request made and server responded");
          console.log(swal.html);
          console.log(error.response);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          //console.log(error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: the request was made but no response was received"
          );
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          swal.html = "Error desconocido. Por favor, avise al Administrador.";
          console.log(
            "Error: something happened in setting up the request that triggered an error"
          );
          console.log(error);
        }
        Swal.fire(swal);
        exito = false;
      });
    return exito;
  };

  async function findBaproSucursal(c_bapro: string, forceUpdate: boolean = false, paramsUpdate: Object = {}): Promise<BaproSucursal | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let baproSucursal: BaproSucursal | undefined = undefined;
    if (isEmpty(baproSucursales.value) || forceUpdate) {
      goFind = await fetchBaproSucursales(paramsUpdate);
    }
    if (goFind) {
      baproSucursal = baproSucursales.value.find((baproSucursal: BaproSucursal) => {
        return baproSucursal.c_bapro === c_bapro;
      });
      if (isEmpty(baproSucursal)) {
        swal.html =
          "No se encontr&oacute; en el sistema la sucursal Bapro buscada. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
    }
    return new Promise((resolve) => {
      resolve((typeof baproSucursal !== "undefined") ? baproSucursal : null);
    });
  };

  return { clearBaproSucursales, getBaproSucursales, fetchBaproSucursales, findBaproSucursal };

});
