import { defineStore } from "pinia";
import http from "@/http";
import type { AxiosResponse } from "axios";
import { urlApi } from "@/shared.js";
// Entities
import Cupo from "@/entities/Cupos/Cupo";
import Cargo from "@/entities/Cupos/Cargo";



export const useCuposStore = defineStore("cuposTope", {
  state: () => ({ listadoCargos: [] as Cargo[], listadoCargosCabecera: [] as Cupo[] }),
  actions: {
    //Select Cargo
    async updateListadoCargos(params?: {
      periodo: string,
      viene_de: string,
    }) {
      const paramsRequest: any = {};
      /*       paramsRequest.periodo = "2023";
            paramsRequest.viene_de = "B"; */
      paramsRequest.periodo = params?.periodo;
      paramsRequest.viene_de = params?.viene_de;
      const response: AxiosResponse<Cargo[]> = await http.request<Cargo[]>({
        method: "GET",
        url: urlApi + "listarCargosParaCupos",
        params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const cargo: Cargo[] = JSON.parse(data);
            const dataAux: Cargo[] = [];
            cargo.forEach((element: Cargo) => {
              dataAux.push(Object.assign(new Cargo(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoCargos = response.data;
    },

    //buscar por id / nombre en usuarios Habilitados (sobre toda la lista)
    /*     async findUsuarioById(
          id_usuario?: number,
          force: boolean = false
        ): Promise<Usuariohabilitado | undefined> {
          if (this.listadoUsuariosHabilitados.length === 0 || force) {
            await this.updateListadoUsuariosHabilitados();
          }
          const usuarioHabilitado = this.listadoUsuariosHabilitados.find(
            (usuarioHabilitado: Usuariohabilitado) => {
              return usuarioHabilitado.id_usuario_perfil == id_usuario;
            }
          );
          return new Promise((resolve) => {
            resolve(usuarioHabilitado);
          });
        }, */

    //listado Cargos Cabecera Grilla 1
    async updateListadoCargosCabecera(params?: {
      id_cargo?: string | number | null,
      id_tabla_sol?: string | number | null,
      tipo_transf: string,
      f_transf_desde: string,
      f_transf_hasta: string,
    }) {
      const paramsRequest: any = {};
      paramsRequest.id_cargo = params?.id_cargo;
      paramsRequest.id_tabla_sol = params?.id_tabla_sol;
      paramsRequest.tipo_transf = params?.tipo_transf;
      paramsRequest.f_transf_desde = params?.f_transf_desde;
      paramsRequest.f_transf_hasta = params?.f_transf_hasta;
      const response: AxiosResponse<Cupo[]> = await http.request<Cupo[]>({
        method: "GET",
        url: urlApi + "listarCuposCabeceras",
        params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const cupo: Cupo[] = JSON.parse(data);
            const dataAux: Cupo[] = [];
            cupo.forEach((element: Cupo) => {
              dataAux.push(Object.assign(new Cupo(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoCargosCabecera = response.data;
    },

  },
});
