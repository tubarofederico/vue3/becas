import { defineStore } from "pinia";
import http from "@/http";
import type { AxiosResponse } from "axios";
import { urlApi } from "@/shared.js";
// Entities
import  Pedido  from "@/entities/Bapro/PedidosCuenta/Pedidocabecera";


export const usepedidosCuentaStore = defineStore("pedidosCuenta", {
  state: () => ({ listadoPedidoCabecera: [] as Pedido[], }),
  actions: {
    async updateListadoCargosCabecera(params?: {
      f_desde:string,
      f_hasta:string,
    }) {
      const paramsRequest: any = {};
      paramsRequest.f_desde = params?.f_desde;
      paramsRequest.f_hasta = params?.f_hasta;
      const response: AxiosResponse<Pedido[]> = await http.request<Pedido[]>({
        method: "GET",
        url: urlApi + "getCabecerasPedidosBapro",
        params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const cabecera: Pedido[] = JSON.parse(data);
            const dataAux: Pedido[] = [];
            cabecera.res.forEach((element: Pedido) => {
              dataAux.push(Object.assign(new Pedido(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoPedidoCabecera = response.data;
    },


  },
});
