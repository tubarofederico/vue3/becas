import { defineStore } from "pinia";
import http from "@/http";
import type { AxiosResponse } from "axios";
import { urlApi } from "@/shared.js";
// Entities
import Cupo from "@/entities/Cupos/Cupo";
import Tipotransf from "@/entities/transferencias/TipoTransf";



export const useTransferenciasStore = defineStore("transferencia", {
  state: () => ({ listadoTransferencias: [] as Cupo[], listadoTiposTransf: [] as Tipotransf[] }),
  actions: {
    //Select Transf
    async updateListadoTransferencias(params?: {
      listar_sin_cupo_por_cargo : string

    }) {
      const paramsRequest: any = {};
      paramsRequest.listar_sin_cupo_por_cargo = params?.listar_sin_cupo_por_cargo;
      const response: AxiosResponse<Tipotransf[]> = await http.request<Tipotransf[]>({
        method: "GET",
        url: urlApi + "listarTiposTransf",
        params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const tipotransf: Tipotransf[] = JSON.parse(data);
            const dataAux: Tipotransf[] = [];
            tipotransf.forEach((element: Tipotransf) => {
              dataAux.push(Object.assign(new Tipotransf(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoTiposTransf = response.data;
    },

    //buscar por id / nombre en usuarios Habilitados (sobre toda la lista)
    /*     async findUsuarioById(
          id_usuario?: number,
          force: boolean = false
        ): Promise<Usuariohabilitado | undefined> {
          if (this.listadoUsuariosHabilitados.length === 0 || force) {
            await this.updateListadoUsuariosHabilitados();
          }
          const usuarioHabilitado = this.listadoUsuariosHabilitados.find(
            (usuarioHabilitado: Usuariohabilitado) => {
              return usuarioHabilitado.id_usuario_perfil == id_usuario;
            }
          );
          return new Promise((resolve) => {
            resolve(usuarioHabilitado);
          });
        }, */

    //listado Transferencias Cabecera Grilla 1
    async updateListadoTransferenciasCabeceras(params?: {
      id_cargo: number | null, //va null
      id_tabla_sol?: number | null,
      tipo_transf: string | null,
      f_transf_desde: string,
      f_transf_hasta: string,
    }) {
      const paramsRequest: any = {};

      if (params !== undefined) {
        if (params.id_tabla_sol !== undefined) {
          paramsRequest.id_tabla_sol = params.id_tabla_sol;
        } else {
          paramsRequest.id_tabla_sol = null;
        }
      }
      paramsRequest.id_cargo = params?.id_cargo;
      paramsRequest.tipo_transf = params?.tipo_transf;
      paramsRequest.f_transf_desde = params?.f_transf_desde;
      paramsRequest.f_transf_hasta = params?.f_transf_hasta;
      const response: AxiosResponse<Cupo[]> = await http.request<Cupo[]>({
        method: "GET",
        url: urlApi + "listarCuposCabeceras",
        params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const cupo: Cupo[] = JSON.parse(data);
            const dataAux: Cupo[] = [];
            cupo.forEach((element: Cupo) => {
              dataAux.push(Object.assign(new Cupo(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoTransferencias = response.data; //falta ver si el listado que manda los datos a mostrar van o no xD
    },

  },
});
