import { defineStore } from "pinia";
import http from "@/http";
// Entities

import type { AxiosResponse } from "axios";
import { urlApi, urlBase } from "@/shared.js";
import type { SweetAlertOptions, SweetAlertResult } from "sweetalert2";
import Swal from "sweetalert2/dist/sweetalert2.js";
import Periodo from "@/entities/Periodo";


export const useConfigStore = defineStore("config", {
  state: () => ({ listadoPeriodoActivo: [] as Periodo[], }),
  actions: {
    //Trae El Periodo Activo
    async updateListadoPeriodo(params?: {

    }) {
      let isSuccess = true;
      const response: AxiosResponse<Periodo[]> | void = await http.request<Periodo[]>({
        method: "GET",
        url: urlApi + "getPeriodoActivo",
        transformResponse: [
          (data: string) => {
            const periodos: Periodo[] = JSON.parse(data);
            const dataAux: Periodo[] = [];
            periodos.res.forEach((element: Periodo) => {
              dataAux.push(Object.assign(new Periodo(), element));
            });
            return dataAux;
          },
        ],
      }).catch(() => {
        Swal.fire({
          icon: "error",
          title: "Error",
          html: "No se encontr&oacute; ning&uacute;n per&iacute;odo en el sistema. Por favor, avise al Administrador."
        }).then(function (result: SweetAlertResult<any>) {
          //if (result.isConfirmed) {
          window.location.replace(urlBase);
          //}
        });
        isSuccess = false;
      });
      this.listadoPeriodoActivo = (isSuccess) ? response.data : [];
    },


  },
});
