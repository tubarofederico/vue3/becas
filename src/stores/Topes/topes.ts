import { defineStore } from "pinia";
import http from "@/http";
import type { AxiosResponse } from "axios";
import  { urlApi } from "@/shared.js";
// Entities
import Tope from "@/entities/Topes/Tope";



export const useTopesStore = defineStore("topes", {
  state: () => ({ listadoTopes: [] as Tope[],}),
  actions: {
 
    //listado Cargos Cabecera Grilla 1
    async updateListadoTopes(params?: {
      id_becario_cupo: number | null,

      fecha_desde:string,
      fecha_hasta:string,
    }) {
      const paramsRequest: any = {};
      paramsRequest.id_becario_cupo = params?.id_becario_cupo;
      paramsRequest.fecha_desde = params?.fecha_desde;
      paramsRequest.fecha_hasta = params?.fecha_hasta;
      const response: AxiosResponse<Tope[]> = await http.request<Tope[]>({
        method: "GET",
        url: urlApi + "getTopesCuposBecarios",
        params: paramsRequest,
        transformResponse: [
          (data: string) => {
            const tope: Tope[] = JSON.parse(data);
            const dataAux: Tope[] = [];
            tope.res.forEach((element: Tope) => {
              dataAux.push(Object.assign(new Tope(), element));
            });
            return dataAux;
          },
        ],
      });
      this.listadoTopes = response.data;
    },

    /* buscar Tope By ID */
    async findTopeById(
      id_becario_cupo?: number,
      force: boolean = false
    ): Promise<Tope | undefined> {
/*       if (this.listadoTopes.length === 0 || force) {
        await this.updateListadoTopes();
      } */
      const topes = this.listadoTopes.find(
        (topes: Tope) => {
          return topes.id_becariocupo == id_becario_cupo;
        }
      );
      return new Promise((resolve) => {
        resolve(topes);
      });
    },

  },
});
