// Librerías
import { defineStore } from "pinia";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { urlApi, isEmpty, checkParams } from "@/shared.js";
// Entidades
import Solicitante from "@/entities/Solicitante";

export const useSolicitantesStore = defineStore("solicitantes", {
  state: () => ({
    solicYProyectos: [] as Solicitante[],
    solicitantes: [] as Solicitante[],
    solicConsulta: [] as Solicitante[]
  }),
  actions: {
    async fetchSolicYProyectos(
      usar_perfil_consulta: number | null = null
    ): Promise<boolean> {
      const swal = {
        icon: "error",
        title: "Error",
        html: "",
      } as SweetAlertOptions;
      let exito: boolean = true;
      let solicYProyectos: Solicitante[] = [];
      this.solicYProyectos = [];
      this.solicitantes = [];
      await http.get<Solicitante[]>(urlApi + "getSolicitantes", {
        params: {
          usar_perfil_consulta: usar_perfil_consulta
        }
      })
        .then(function (response: AxiosResponse<Solicitante[]>) {
          response.data.forEach((solicOProyecto: Solicitante) => {
            solicYProyectos.push(Object.assign(new Solicitante(), solicOProyecto)); // esta serie de instanciaciones es necesaria debido a que este listado se utiliza en un autocompletar
          });
        })
        .catch(function (error: AxiosError<string>) {
          if (error.response) {
            // Request made and server responded
            swal.html =
              error.message +
              " (" +
              error.code +
              ")." +
              "<br />" +
              error.response.data.substring(
                error.response.data.indexOf("<h2>") + 4,
                error.response.data.indexOf("</h2>")
              );
            console.log("Error: request made and server responded");
            console.log(swal.html);
            console.log(error.response);
            //console.log(error.response.status);
            //console.log(error.response.headers);
            //console.log(error.response.data);
          } else if (error.request) {
            // The request was made but no response was received
            swal.html = "Error desconocido. Por favor, avise al Administrador.";
            console.log(
              "Error: the request was made but no response was received"
            );
            console.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            swal.html = "Error desconocido. Por favor, avise al Administrador.";
            console.log(
              "Error: something happened in setting up the request that triggered an error"
            );
            console.log(error);
          }
          Swal.fire(swal);
          exito = false;
        });
      this.solicYProyectos = solicYProyectos;
      exito = !isEmpty(this.solicYProyectos) ? this.updateSolicitantes() : exito;
      return exito;
    },
    updateSolicitantes(): boolean {
      const swal = {
        icon: "error",
        title: "Error",
        html: "No se encontr&oacute; ning&uacute;n solicitante en el sistema. Por favor, avise al Administrador.",
      } as SweetAlertOptions;
      let exito: boolean;
      this.solicitantes = this.solicYProyectos.filter(solicOProyecto => solicOProyecto.proyecto === 0);
      exito = !isEmpty(this.solicitantes);
      if (!exito) {
        Swal.fire(swal);
      }
      return exito;
    },
    async fetchSolicConsulta(paramsRequest: Object): Promise<boolean> {
      const allParamsNames: String[] = ["listar_proyectos", "periodo", "id_cargo", "id_tabla_sol", "estado", "usar_perfil_consulta"];
      const swal = {
        icon: "error",
        title: "Error",
        html: "",
      } as SweetAlertOptions;
      let exito: boolean = true;
      let solicConsulta: Solicitante[] = [];
      this.solicConsulta = [];
      if (!checkParams(paramsRequest, allParamsNames)) {
        swal.html = "Los par&aacute;metros enviados para realizar la consulta de los solicitantes son incorrectos. Por favor, avise al Administrador.";
        Swal.fire(swal);
        return false;
      }
      await http.get<Solicitante[]>(urlApi + "getSolicitantes", { params: paramsRequest })
        .then(function (response: AxiosResponse<Solicitante[]>) {
          solicConsulta = response.data;
        })
        .catch(function (error: AxiosError<string>) {
          if (error.response) {
            // Request made and server responded
            swal.html = error.response.data;
            console.log("Error: request made and server responded");
            console.log(swal.html);
            console.log(error.response);
            //console.log(error.response.status);
            //console.log(error.response.headers);
            //console.log(error.response.data);
          } else if (error.request) {
            // The request was made but no response was received
            swal.html = "Error desconocido. Por favor, avise al Administrador.";
            console.log("Error: the request was made but no response was received");
            console.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            swal.html = "Error desconocido. Por favor, avise al Administrador.";
            console.log("Error: something happened in setting up the request that triggered an error");
            console.log(error);
          }
          Swal.fire(swal);
          exito = false;
        });
      this.solicConsulta = solicConsulta;
      return exito;
    },
    async findSolicOProyecto(
      id_tabla_sol: number,
      forceUpdate: boolean = false
    ): Promise<Solicitante | null> {
      const swal: SweetAlertOptions = {
        icon: "error",
        title: "Error",
        html: ""
      };
      let goFind: boolean = true;
      let solicOProyecto: Solicitante | undefined = undefined;
      if (isEmpty(this.solicYProyectos) || forceUpdate) {
        goFind = await this.fetchSolicYProyectos();
      }
      if (goFind) {
        solicOProyecto = this.solicYProyectos.find((solicOProyecto: Solicitante) => {
          return solicOProyecto.id_tabla_sol === id_tabla_sol;
        });
        if (isEmpty(solicOProyecto)) {
          swal.html =
            "No se encontr&oacute; en el sistema el solicitante o proyecto buscado. Por favor, avise al Administrador.";
          Swal.fire(swal);
        }
      }
      return new Promise((resolve) => {
        resolve((typeof solicOProyecto !== "undefined") ? solicOProyecto : null);
      });
    },
    async findSolicitante(
      id_tabla_sol: number,
      forceUpdate: boolean = false
    ): Promise<Solicitante | null> {
      const swal: SweetAlertOptions = {
        icon: "error",
        title: "Error",
        html: ""
      };
      let goFind: boolean = true;
      let solicitante: Solicitante | undefined = undefined;
      if (isEmpty(this.solicitantes) || forceUpdate) {
        // goFind = await this.fetchSolicitantes();
        goFind = await this.fetchSolicYProyectos();
      }
      if (goFind) {
        solicitante = this.solicitantes.find((solicitante: Solicitante) => {
          return solicitante.id_tabla_sol === id_tabla_sol;
        });
        if (isEmpty(solicitante)) {
          swal.html =
            "No se encontr&oacute; en el sistema el solicitante buscado. Por favor, avise al Administrador.";
          Swal.fire(swal);
        }
      }
      return new Promise((resolve) => {
        resolve((typeof solicitante !== "undefined") ? solicitante : null);
      });
    }
  }
});
