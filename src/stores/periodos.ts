import { defineStore } from "pinia";
import { ref } from "vue";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { AxiosResponse, AxiosError } from "axios";
import type { SweetAlertOptions } from "sweetalert2";
import { urlApi, isEmpty } from "@/shared.js";

// Entities
import Periodo from "@/entities/Periodo";

export const usePeriodosStore = defineStore("periodos", () => {

  const periodos = ref({
    listado: [] as Periodo[],
    anual: [] as Periodo[],
    activo: null as Periodo | null
  });

  function findPeriodoActivo(): Periodo | null {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    const periodo: Periodo | undefined = periodos.value.listado.find((periodo: Periodo) => {
      return periodo.estado === "A";
    });
    if (isEmpty(periodo)) {
      swal.html =
        "No se encontr&oacute; en el sistema el per&iacute;odo activo. Por favor, avise al Administrador.";
      Swal.fire(swal);
    }
    return (typeof periodo !== "undefined") ? periodo : null;
  }

  function filterPeriodosAnual(): Periodo[] {
    const swal = {
      icon: "error",
      title: "Error",
      html: "No se encontr&oacute; ning&uacute;n per&iacute;odo en el sistema. Por favor, avise al Administrador.",
    } as SweetAlertOptions;
    const periodosAnual: Periodo[] = periodos.value.listado.filter((value, index, self) => {
      return self.findIndex(v => v.anioActivo === value.anioActivo) === index;
    });
    if (isEmpty(periodosAnual)) {
      Swal.fire(swal);
    }
    return periodosAnual;
  }

  async function fetchPeriodos(forceUpdate: boolean = false): Promise<boolean> {
    const swal = {
      icon: "error",
      title: "Error",
      html: "",
    } as SweetAlertOptions;
    let exito: boolean = true;
    if (isEmpty(periodos.value.listado) || forceUpdate) {
      periodos.value.listado = [];
      periodos.value.anual = [];
      periodos.value.activo = null;
      await http.get<Periodo[]>(urlApi + "getPeriodos")
        .then(function (response: AxiosResponse<Periodo[]>) {
          periodos.value.listado = response.data;
          periodos.value.anual = filterPeriodosAnual();
          periodos.value.activo = findPeriodoActivo();
        })
        .catch(function (error: AxiosError<string>) {
          if (error.response) {
            // Request made and server responded
            swal.html =
              error.message +
              " (" +
              error.code +
              ")." +
              "<br />" +
              error.response.data.substring(
                error.response.data.indexOf("<h2>") + 4,
                error.response.data.indexOf("</h2>")
              );
            console.log("Error: request made and server responded");
            console.log(swal.html);
            console.log(error.response);
            //console.log(error.response.status);
            //console.log(error.response.headers);
            //console.log(error.response.data);
          } else if (error.request) {
            // The request was made but no response was received
            swal.html = "Error desconocido. Por favor, avise al Administrador.";
            console.log(
              "Error: the request was made but no response was received"
            );
            console.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            swal.html = "Error desconocido. Por favor, avise al Administrador.";
            console.log(
              "Error: something happened in setting up the request that triggered an error"
            );
            console.log(error);
          }
          Swal.fire(swal);
          exito = false;
        });
    }
    return exito;
  };

  async function findPeriodo(id_periodo: number, forceUpdate: boolean = false): Promise<Periodo | null> {
    const swal: SweetAlertOptions = {
      icon: "error",
      title: "Error",
      html: ""
    };
    let goFind: boolean = true;
    let periodo: Periodo | undefined = undefined;
    goFind = await fetchPeriodos(forceUpdate);
    if (goFind) {
      periodo = periodos.value.listado.find((periodo: Periodo) => {
        return periodo.id_periodo === id_periodo;
      });
      if (isEmpty(periodo)) {
        swal.html =
          "No se encontr&oacute; en el sistema el per&iacute;odo buscado. Por favor, avise al Administrador.";
        Swal.fire(swal);
      }
    }
    return new Promise((resolve) => {
      resolve((typeof periodo !== "undefined") ? periodo : null);
    });
  };

  return { periodos, fetchPeriodos, findPeriodo };

});
