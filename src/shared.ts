import { Loading } from "@hcd-bsas/vue3-components";
import { BreadCrumbKey } from "@hcd-bsas/vue3-components";
import { inject } from "vue";

// CONSTANTES
export const urlBase = import.meta.env.VITE_URL_BASE;
export const urlComun = import.meta.env.VITE_URL_COMUN;
export const urlApi = import.meta.env.VITE_URL_API;
export const urlSeguridad = import.meta.env.VITE_URL_SEGURIDAD;


// FUNCIONES
export function getUrl(path: string, params: string[] = [], queryObjectParams: any = {}): string {
  var queryParams = '';
  if (!isEmpty(queryObjectParams)) {
    var urlSearchParams: URLSearchParams = new URLSearchParams(queryObjectParams);
    queryParams = '?' + urlSearchParams.toString();
  }
  return (
    import.meta.env.VITE_URL_API +
    path +
    (params.length > 0 ? "/" + params.join("/") : "")
    + queryParams
  );
}

export function isEmpty(data: any): boolean {
  try {
    return (
      (data == null) ||
      (typeof data === "undefined") ||
      (typeof data === "string" && data.trim() === "") ||
      (typeof data === "number" && Number.isNaN(data)) ||
      (Array.isArray(data) && data.length == 0) ||
      (typeof data === "object" &&
        data &&
        Object.keys(data).length === 0 &&
        Object.getPrototypeOf(data) === Object.prototype)
    );
  } catch (error) {
    console.log(error);
  }
  return false;
}

export function getRouteNumberParam(param: string | string[]): number {
  let rta: number;
  if (typeof param === "string") {
    rta = parseInt(param);
  } else if (typeof param === "object") {
    rta = parseInt(param[0]);
  } else {
    throw new Error("Imposible parsear el parámetro");
  }
  return rta;
}

export function isNumber(value: string) {
  return /^\d*$/.test(value);
}

export function isPair(value: number): boolean {
  return value % 2 == 0;
}

export function checkParams(
  paramsRequest: Object,
  allParamsNames: String[]
): boolean {
  const paramsRequestNames: String[] =
    Object.getOwnPropertyNames(paramsRequest);
  const mergedNames: String[] = [
    ...new Set([...allParamsNames, ...paramsRequestNames]),
  ];
  const equalsCheck: (a: String[], b: String[]) => boolean = (a, b) =>
    a.length === b.length && a.every((v, i) => v === b[i]);
  return equalsCheck(allParamsNames, mergedNames);
}

// LIBRERIAS
export const loading = new Loading();

export const useBreadcrumb = () => {
  const breadcrumb = inject(BreadCrumbKey)?.value;

  return { breadcrumb };
};
